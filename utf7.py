# Copyright (c) Twisted Matrix Laboratories.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# code excerpt from src/twisted/mail/imap4.py, dbc7235

import codecs

# we need to cast Python >=3.3 memoryview to chars (from unsigned bytes), but
# cast is absent in previous versions: thus, the lambda returns the
# memoryview instance while ignoring the format
memory_cast = getattr(memoryview, "cast", lambda *x: x[0])

def modified_base64(s):
    s_utf7 = s.encode("utf-7")
    return s_utf7[1:-1].replace(b"/", b",")


def modified_unbase64(s):
    s_utf7 = b"+" + s.replace(b",", b"/") + b"-"
    return s_utf7.decode("utf-7")


def encoder(s, errors=None):
    """
    Encode the given C{unicode} string using the IMAP4 specific variation of
    UTF-7.

    @type s: C{unicode}
    @param s: The text to encode.

    @param errors: Policy for handling encoding errors.  Currently ignored.

    @return: L{tuple} of a L{str} giving the encoded bytes and an L{int}
        giving the number of code units consumed from the input.
    """
    r = bytearray()
    _in = []
    valid_chars = set(map(chr, range(0x20, 0x7F))) - {"&"}
    for c in s:
        if c in valid_chars:
            if _in:
                r += b"&" + modified_base64("".join(_in)) + b"-"
                del _in[:]
            r.append(ord(c))
        elif c == "&":
            if _in:
                r += b"&" + modified_base64("".join(_in)) + b"-"
                del _in[:]
            r += b"&-"
        else:
            _in.append(c)
    if _in:
        r.extend(b"&" + modified_base64("".join(_in)) + b"-")
    return (bytes(r), len(s))


def decoder(s, errors=None):
    """
    Decode the given L{str} using the IMAP4 specific variation of UTF-7.

    @type s: L{str}
    @param s: The bytes to decode.

    @param errors: Policy for handling decoding errors.  Currently ignored.

    @return: a L{tuple} of a C{unicode} string giving the text which was
        decoded and an L{int} giving the number of bytes consumed from the
        input.
    """
    r = []
    decode = []
    s = memory_cast(memoryview(s), "c")
    for c in s:
        if c == b"&" and not decode:
            decode.append(b"&")
        elif c == b"-" and decode:
            if len(decode) == 1:
                r.append("&")
            else:
                r.append(modified_unbase64(b"".join(decode[1:])))
            decode = []
        elif decode:
            decode.append(c)
        else:
            r.append(c.decode())
    if decode:
        r.append(modified_unbase64(b"".join(decode[1:])))
    return ("".join(r), len(s))


class StreamReader(codecs.StreamReader):
    def decode(self, s, errors="strict"):
        return decoder(s)


class StreamWriter(codecs.StreamWriter):
    def encode(self, s, errors="strict"):
        return encoder(s)


_codecInfo = codecs.CodecInfo(encoder, decoder, StreamReader, StreamWriter)


def imap4_utf_7(name):
    # In Python 3.9, codecs.lookup() was changed to normalize the codec name
    # in the same way as encodings.normalize_encoding().  The docstring
    # for encodings.normalize_encoding() describes how the codec name is

    # normalized.  We need to replace '-' with '_' to be compatible with
    # older Python versions.
    #  See:  https://bugs.python.org/issue37751
    #        https://github.com/python/cpython/pull/17997
    if name.replace("-", "_") == "imap4_utf_7":
        return _codecInfo
