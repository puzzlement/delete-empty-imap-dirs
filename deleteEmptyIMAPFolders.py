#!/usr/bin/env python

import argparse
import codecs
import getpass
import imaplib
import logging
import sys

import parseNested
import utf7

codecs.register(utf7.imap4_utf_7)

IGNORE = set(["INBOX", "Postponed", "Sent", "Sent Items", "Trash", "Drafts", "MQEmail.INBOX", "MQEmail.Outbox", "MQEmail.Postponed"])

def main():
    parser = argparse.ArgumentParser(
            description='This script deletes empty remote IMAP folders.')
    parser.add_argument("-n", "--dry-run",
                      action="store_true", dest="dryrun", default=False,
                      help="Dry run (deletions listed at log level WARN, but not executed)")
    parser.add_argument('--log', default = logging.INFO,
                        choices=logging.getLevelNamesMapping().keys(),
                        help="Set log level (default: %s)" %
                            logging.getLevelName(logging.INFO))
    parser.add_argument('--port', '-p', metavar='PORT', type = int,
            help = 'Port number to connect to (143 or 993/SSL used by default)')
    parser.add_argument("-s", "--ssl",
                      action="store_true", dest="ssl", default=False,
                      help="Use SSL encryption")
    parser.add_argument('hostname', help="Domain name/host name of IMAP "
            "server to delete folders on")
    parser.add_argument('username', help="Username/login "
            "on IMAP server")
    args = parser.parse_args()

    logging.basicConfig(level=args.log)

    if args.dryrun:
        logging.debug("Dry run mode set, mailboxes will not be deleted")
    else:
        logging.debug("Dry run mode not set, mailboxes will be deleted")

    if args.ssl:
        IMAPClass = imaplib.IMAP4_SSL
    else:
        IMAPClass = imaplib.IMAP4
    if args.port:
        M = IMAPClass(args.hostname, args.port)
    else:
        M = IMAPClass(args.hostname)
    M.login(args.username, getpass.getpass('IMAP password for user %s at server %s: ' % (args.username, args.hostname)))
    listresponse = M.list()
    mailboxes = []
    for chunk in listresponse[1]:
        nested = parseNested.parseNestedParens(chunk.decode('imap4-utf-7'))
        if '\\HasNoChildren' in nested[0] and '\\NoSelect' in nested[0]:
            if args.dryrun:
                logging.warn("Dry run: mailbox %s has no children and is not selectable, would be deleted" % nested[2])
            else:
                logging.info("Mailbox %s has no children and is not selectable, deleting now"  % nested[2])
                M.delete(nested[2])
        
        elif not '\\HasChildren' in nested[0]:
            mboxname = nested[2]
            ignoretest = set([])
            ignoretest.add(mboxname)
            ignoretest.add("INBOX." + mboxname)
            ignoretest.add(mboxname.lstrip("INBOX."))
            if not ignoretest.intersection(IGNORE):
                mailboxes.append(mboxname)
            else:
                logging.debug("%s is excluded from deletion, ignoring" % mboxname)
    for mailbox in mailboxes:
        reply, data = M.select(mailbox)
        if reply != 'OK':
            logging.error("Cannot select mailbox '%s', reply was '%s', skipping" % (mailbox, str(data[0])))
            continue
        else:
            nomessages = int(data[0])
            M.close()
            if nomessages == 0:
                if args.dryrun:
                    logging.warn("Dry run: mailbox %s has no messages, would be deleted" % nested[2])
                else:
                    logging.info("Mailbox %s has no messages, deleting now" % nested[2])
                    M.delete(nested[2])
            else:
                logging.debug("%s contains %d messages, NOT deleting" % (mailbox, nomessages))
    M.logout()

if __name__ == '__main__':
    main()
