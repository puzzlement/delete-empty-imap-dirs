Delete empty IMAP dirs
-----------------------

Users of any mail sorting recipe that creates new mail folders a lot tend to
find that over time they accumulate a lot of mail folders for, eg, email lists
they are no longer subscribed to. And most IMAP clients will waste time
checking those folders for new mail all the time.

This script deletes empty remote IMAP folders.

Requirements
-------------

* Python 3

Usage
-----

```
usage: deleteEmptyIMAPFolders.py [-h] [-n] [--log {CRITICAL,FATAL,ERROR,WARN,WARNING,INFO,DEBUG,NOTSET}] [--port PORT] [-s] hostname username

This script deletes empty remote IMAP folders.

positional arguments:
  hostname              Domain name/host name of IMAP server to delete folders on
  username              Username/login on IMAP server

options:
  -h, --help            show this help message and exit
  -n, --dry-run         Dry run (deletions listed at log level WARN, but not executed)
  --log {CRITICAL,FATAL,ERROR,WARN,WARNING,INFO,DEBUG,NOTSET}
                        Set log level (default: INFO)
  --port, -p PORT       Port number to connect to (143 or 993/SSL used by default)
  -s, --ssl             Use SSL encryption
```

About
=====

Credits
-------

Delete empty IMAP dirs was developed by [Mary
Gardiner](https://mary.gardiner.id.au/). Code within the `parseNested.py` and
`utf7.py` files is from [Twisted](https://twisted.org/), see the file header
for full credits.

Licence
-------

Delete empty IMAP dirs is free software available under the MIT licence. See
LICENCE for full details.
